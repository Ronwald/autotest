package pageObjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import utilities.MyUtilities;

public class DP_Home {
	
	
	private static WebElement element = null;
	
	public static WebElement ClickLink (WebDriver driver, String linkname, ExtentTest test) throws IOException {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		element = driver.findElement(By.linkText(linkname));	
		
		List<WebElement> elements = driver.findElements(By.linkText(linkname));
		
		int size = elements.size();
		//System.out.println(size);
		
		if(size>0) {
			test.log(Status.PASS, "Step passed. Link : " + linkname + " is clicked");
	    	elements.get(0).click();
	    	
	    }else {
	    	test.log(Status.FAIL, "Step failed. Link : " + linkname + " is not clicked. CLosing this test.");
	    	MyUtilities.takeScreenshot(driver, test);
	    	driver.quit();
	    	    	
	    }
		
		return element;
		
	}

}

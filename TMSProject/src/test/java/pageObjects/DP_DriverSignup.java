package pageObjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import utilities.MyUtilities;

public class DP_DriverSignup {
	
	//Ronwald Sandoval [March 3 2020]
	//this page is for the driver signup page
	//link : https://dp-test.urbanfox.asia/delivery-person/signup
	
	
	private static WebElement myElements;
	
	
	
	public static void SelectBirthDate(WebDriver driver, int textSeries, String fieldName, String myDate, ExtentTest test) throws IOException {	

		//List<WebElement> elements = driver.findElements(By.xpath("(//input[@type='text'" +  "])" + "[" + textSeries + "]" ));
	    myElements = driver.findElement(By.xpath("(//input[@type='text'" +  "])" + "[" + textSeries + "]" ));
	    myElements.click();
	    
	    MyUtilities.myWait(1000);
		
		String[] dateArray;
		dateArray = myDate.split("/");

		String myDay = dateArray[0].trim();
		String myMonth = dateArray[1].trim();
		String myYear = dateArray[2].trim();

		
		switch (myMonth) {
		  case "01":
			//January, cos the 'value' attribute is 0  
			myMonth = "0";
		    break;
		  case "02":
			myMonth = "1";
			break;
		  case "03":
			myMonth = "2";
			break;
		  case "04":
		    myMonth = "3";
			break;
		  case "05":
			myMonth = "4";
		    break;
		  case "06":
			myMonth = "5";
			break;
		  case "07":
			myMonth = "6";
			break;
		  case "08":
		    myMonth = "7";
			break;
		  case "09":
			myMonth = "8";
		    break;
		  case "10":
			myMonth = "9";
			break;
		  case "11":
			myMonth = "10";
			break;
		  case "12":
		    myMonth = "11";
			break;  
	
		}
		
		
		//select year
		Select drpYear = new Select(driver.findElement(By.className("yearselect")));
		MyUtilities.myWait(1000);
		
		if(drpYear.getOptions().size()>1) {
			drpYear.selectByValue(myYear);		
		}
						
		//select month
		Select drpMonth = new Select(driver.findElement(By.className("monthselect")));
		MyUtilities.myWait(1000);
		if(drpMonth.getOptions().size()> 1) {	
			drpMonth.selectByValue(myMonth);		
		}
		
		MyUtilities.myWait(1000);	
		
		//click the day
		List<WebElement> alldates = driver.findElements(By.xpath("//table[@class='table-condensed']//td"));		
		for(WebElement ele: alldates)
		{		
			String date=ele.getText();	
			if(date.equalsIgnoreCase(myDay))
			{
				ele.click();			
				break;
			}
			
		}
		
		
	}
	
	
	public static void clickRadioButtonGender(WebDriver driver, String gender, ExtentTest test) throws IOException {	
		
		
		if(gender.equalsIgnoreCase("Male")) {
			//WebElement myGender = driver.findElement(By.xpath("//div[2]/div[2]/div/fieldset/div[2]/div/label"));	
			WebElement myGender = driver.findElement(By.xpath("//*[text()='Male']"));	
				
			if(myGender.isEnabled()) {
				myGender.click();
				test.log(Status.PASS, "Step passed. Radio button for gender : " + gender + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for gender : " + gender + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(gender.equalsIgnoreCase("Female")) {
			WebElement myGender = driver.findElement(By.xpath("//div[2]/div[2]/div/fieldset/div[2]/div[2]/label"));	
			//WebElement myGender = driver.findElement(By.xpath("//*[text()='Female']"));	
			
			if(myGender.isEnabled()) {
				myGender.click();
				test.log(Status.PASS, "Step passed. Radio button for gender : " + gender + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for gender : " + gender + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}else {
			
			WebElement myGender = driver.findElement(By.xpath("//div[2]/div[3]/label"));
			//WebElement myGender = driver.findElement(By.xpath("//*[text()='Female']"));
			
			if(myGender.isEnabled()) {
				myGender.click();
				test.log(Status.PASS, "Step passed. Radio button for gender : " + gender + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for gender : " + gender + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
		
	}
	
	
	public static void clickRadioButtonRace(WebDriver driver, String race, ExtentTest test) throws IOException {
		
		String myRace = race.trim();
		
		switch (myRace) {
		  case "Chinese":
			  
			  //WebElement myRace1 = driver.findElement(By.xpath("//fieldset/div[3]/div/label"));	
			  WebElement myRace1 = driver.findElement(By.xpath("//*[text()='Chinese']"));
				
			  if(myRace1.isEnabled()) {
				  myRace1.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }
		  break;
		    
		  case "Malay":
			  WebElement myRace2 = driver.findElement(By.xpath("//*[text()='Malay']"));	
				
			  if(myRace2.isEnabled()) {
				  myRace2.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }	
		  break;
		  
		  case "Indian":
			  WebElement myRace3 = driver.findElement(By.xpath("//*[text()='Indian']"));	
				
			  if(myRace3.isEnabled()) {
				  myRace3.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }	
			
		  break;
		  
		  case "Eurasian":
			  WebElement myRace4 = driver.findElement(By.xpath("//*[text()='Eurasian']"));	
				
			  if(myRace4.isEnabled()) {
				  myRace4.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }	
		    
		  break;
		  
		  case "Others":
			  WebElement myRace5 = driver.findElement(By.xpath("//*[text()='Others']"));	
				
			  if(myRace5.isEnabled()) {
				  myRace5.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }
			    
		  break;
		  
		  default:
			  WebElement myRace6 = driver.findElement(By.xpath("//div[6]/label"));	
				
			  if(myRace6.isEnabled()) {
				  myRace6.click();
					test.log(Status.PASS, "Step passed. Radio button for race : " + race + " is clicked" );		
			  }else {
					test.log(Status.FAIL, "Step failed. Radio button for race : " + race + " is not displayed. Closing this test.");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	driver.quit(); 
			  }
			  
	
		}
		
		
	}
	

	public static void clickRadioButtonCitizenship(WebDriver driver, String citizenship, ExtentTest test) throws IOException {	
		
		
		if(citizenship.equalsIgnoreCase("Yes, Singaporean")) {
			WebElement isAuthorised = driver.findElement(By.xpath("//div[4]/div/label"));	
			
			if(isAuthorised.isEnabled()) {
				isAuthorised.click();
				test.log(Status.PASS, "Step passed. Radio button for legal to work option : " + citizenship + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for legal to work option : " + citizenship + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(citizenship.equalsIgnoreCase("Yes, Permanent Resident")) {
			WebElement isAuthorised = driver.findElement(By.xpath("//div[4]/div[2]/label"));	
			
			if(isAuthorised.isEnabled()) {
				isAuthorised.click();
				test.log(Status.PASS, "Step passed. Radio button for legal to work option : " + citizenship + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for legal to work option : " + citizenship + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}else {
			
			WebElement isAuthorised = driver.findElement(By.xpath("//div[4]/div[3]/label"));	
			
			if(isAuthorised.isEnabled()) {
				isAuthorised.click();
				test.log(Status.PASS, "Step passed. Radio button for legal to work option : " + citizenship + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for legal to work option : " + citizenship + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
		
	}
	
	
	public static void clickRadioButtonMobileType(WebDriver driver, String mobileType, ExtentTest test) throws IOException {	
		
		
		if(mobileType.equalsIgnoreCase("Apple iPhone")) {
			WebElement mobileDevice = driver.findElement(By.xpath("//div[3]/div/div/fieldset/div/div/label"));	
			
			if(mobileDevice.isEnabled()) {
				mobileDevice.click();
				test.log(Status.PASS, "Step passed. Radio button for Mobile device option : " + mobileType + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for Mobile device option : " + mobileType + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(mobileType.equalsIgnoreCase("Android phone")) {
			WebElement mobileDevice = driver.findElement(By.xpath("//div[3]/div/div/fieldset/div/div[2]/label"));	
			
			if(mobileDevice.isEnabled()) {
				mobileDevice.click();
				test.log(Status.PASS, "Step passed. Radio button for Mobile device option : " + mobileType + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for Mobile device option : " + mobileType + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}else {
			
			WebElement mobileDevice = driver.findElement(By.xpath("//div[3]/div/div/fieldset/div/div[3]/label"));	
			
			if(mobileDevice.isEnabled()) {
				mobileDevice.click();
				test.log(Status.PASS, "Step passed. Radio button for Mobile device option : " + mobileType + " is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button for Mobile device option : " + mobileType + " is not displayed. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
		
	}
	
	
	public static void selectBankName(WebDriver driver, String bankName, ExtentTest test) throws IOException {
		
		//Select drpBankName = new Select(driver.findElement(By.className("bank_name")));
		Select drpBankName = new Select(driver.findElement(By.name("bank_name")));
		WebElement firstOption = drpBankName.getFirstSelectedOption();
		
		if(firstOption.isEnabled()) {
			drpBankName.selectByVisibleText(bankName.trim());
			test.log(Status.PASS, "Step passed. Bank name option : " + bankName + " is selected" );		
		}else {
			test.log(Status.FAIL, "Step failed. Bank name option :  " + bankName + " is not displayed. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	driver.quit(); 
		}
		
		
	}


	public static void SelectTrainingDate(WebDriver driver, int textSeries, String fieldName, String myDate, ExtentTest test) throws IOException {	

		myElements = driver.findElement(By.xpath("(//input[@type='text'" +  "])" + "[" + textSeries + "]" ));
	    //myElements = driver.findElement(By.xpath("//div/div/span/input"));
	    myElements.click();
	    
		MyUtilities.myWait(1000);
	
		String[] dateArray;
		dateArray = myDate.split("/");

		String myDay = dateArray[0].trim();
		String myMonth = dateArray[1].trim();
		String myYear = dateArray[2].trim();

		
		
		switch (myMonth) {
		  case "01":
			//January, cos the 'value' attribute is 0  
			myMonth = "0";
		    break;
		  case "02":
			myMonth = "1";
			break;
		  case "03":
			myMonth = "2";
			break;
		  case "04":
		    myMonth = "3";
			break;
		  case "05":
			myMonth = "4";
		    break;
		  case "06":
			myMonth = "5";
			break;
		  case "07":
			myMonth = "6";
			break;
		  case "08":
		    myMonth = "7";
			break;
		  case "09":
			myMonth = "8";
		    break;
		  case "10":
			myMonth = "9";
			break;
		  case "11":
			myMonth = "10";
			break;
		  case "12":
		    myMonth = "11";
			break;  
	
		}
		
		
		//select year
		Select drpYear = new Select(driver.findElement(By.xpath("//div[6]/div[1]/div[2]/table/thead/tr[1]/th[2]/select[2]")));
		MyUtilities.myWait(1000);
		if(drpYear.getOptions().size()>1) {
			drpYear.selectByValue(myYear);		
		}

		//select month
		Select drpMonth = new Select(driver.findElement(By.xpath("//div[6]/div[1]/div[2]/table/thead/tr[1]/th[2]/select[1]")));
		MyUtilities.myWait(1000);
		if(drpMonth.getOptions().size()> 1) {			
			drpMonth.selectByValue(myMonth);		
		}
		
		MyUtilities.myWait(1000);	
		
		//click the day
		List<WebElement> alldates = driver.findElements(By.xpath("//table[@class='table-condensed']//td"));		
		for(WebElement ele: alldates)
		{		
			String date=ele.getText();		
			if(date.equalsIgnoreCase(myDay))
			{
				ele.click();		
				break;
			}
			
		}
		
		
	}
	
	
	public static void selectFulfill(WebDriver driver, String transpoType, ExtentTest test) {
		
	
		List<WebElement> allTranspo = driver.findElements(By.xpath("//div[3]/div[2]/div/fieldset/div/div/label"));
		
		for(WebElement ele: allTranspo) {
			String myTranspo = ele.getText();	
			if(myTranspo.trim().equalsIgnoreCase(transpoType)) {
				ele.click();
				test.log(Status.PASS, "Step passed. Fulfillment delivery option : " + transpoType + " is selected" );	
				break;
			}
			
		}
		
		
	}


	public static void EnterText (WebDriver driver, String fieldname, String valueToEnter, ExtentTest test) throws IOException {	
		//List<WebElement> elements = driver.findElements(By.linkText(linkText));	
		
		List<WebElement> elements = driver.findElements(By.name(fieldname));
		
		int size = elements.size();
		
		
		if(size>0) {
			elements.get(0).sendKeys(valueToEnter);	
			test.log(Status.PASS, "Step passed. Value :  "  + "'" + valueToEnter + "'" + "  for field :  " + "'" + fieldname + "'" + "  is entered");
	    	
	    }else {
	    	test.log(Status.FAIL, "Step FAILED. Value :  "  + "'" + valueToEnter + "'" + "  for field :  " + "'" + fieldname + "'" + "  is entered");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	   	
	    }		

	}

	
	public static void clickRadioButtonConvicted(WebDriver driver, String isConvicted, ExtentTest test) throws IOException {	
			
		if(isConvicted.trim().equalsIgnoreCase("No")) {
			WebElement isNo = driver.findElement(By.xpath("//fieldset/div/div/div/label"));	
			
			if(isNo.isEnabled()) {
				isNo.click();
				test.log(Status.PASS, "Step passed. Radio button :  "  + "'" + isConvicted + "'" + "  is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button :  "  + "'" + isConvicted + "'" + "  is not clicked");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(isConvicted.trim().equalsIgnoreCase("Yes")) {
			WebElement isYes = driver.findElement(By.xpath("//div/div/div[2]/label"));	
			
			if(isYes.isEnabled()) {
				isYes.click();
				test.log(Status.PASS, "Step passed. Radio button :  "  + "'" + isConvicted + "'" + "  is clicked" );		
			}else {
				test.log(Status.FAIL, "Step failed. Radio button :  "  + "'" + isConvicted + "'" + "  is not clicked");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
	}
	
	
	public static void clickRadioButtonCriminal(WebDriver driver, String isCriminal, ExtentTest test) throws IOException {	
		
		if(isCriminal.trim().equalsIgnoreCase("No")) {
			WebElement isNo = driver.findElement(By.xpath("//div[2]/div/div/label"));	
			
			if(isNo.isEnabled()) {
				isNo.click();
				test.log(Status.PASS, "Step passed. Option :  "  + "'" + isCriminal + "'" + "  is selected for 'Have you ever been convicted' " );		
			}else {
				test.log(Status.FAIL, "Step failed. Option :  "  + "'" + isCriminal + "'" + "  is not selected for 'Have you ever been convicted' ");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(isCriminal.trim().equalsIgnoreCase("Yes")) {
			WebElement isYes = driver.findElement(By.xpath("//div[2]/div/div[2]/label"));	
			
			if(isYes.isEnabled()) {
				isYes.click();
				test.log(Status.PASS, "Step passed. Option :  "  + "'" + isCriminal + "'" + "  is selected for 'DO you have any civil or criminal case' " );		
			}else {
				test.log(Status.FAIL, "Step failed. Option :  "  + "'" + isCriminal + "'" + "  is not selected for 'DO you have any civil or criminal case' ");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
	}
	
	
	public static void clickRadioButtonTreatment(WebDriver driver, String hasTreatment, ExtentTest test) throws IOException {	
		
		if(hasTreatment.trim().equalsIgnoreCase("No")) {
			WebElement isNo = driver.findElement(By.xpath("//div[6]/div/div/fieldset/div[3]/div/label"));	
			
			if(isNo.isEnabled()) {
				isNo.click();
				test.log(Status.PASS, "Step passed. Option :  "  + "'" + hasTreatment + "'" + "  is selected for 'Have you undergone treatment' " );		
			}else {
				test.log(Status.FAIL, "Step failed. Option :  "  + "'" + hasTreatment + "'" + "  is not selected for 'Have you undergone treatment' ");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}
			
			
		}else if(hasTreatment.trim().equalsIgnoreCase("Yes")) {
			WebElement isYes = driver.findElement(By.xpath("//div[6]/div/div/fieldset/div[3]/div[2]/label"));	
			
			if(isYes.isEnabled()) {
				isYes.click();
				test.log(Status.PASS, "Step passed. Option :  "  + "'" + hasTreatment + "'" + "  is selected for 'DO you have undergone treatment' " );		
			}else {
				test.log(Status.FAIL, "Step failed. Option :  "  + "'" + hasTreatment + "'" + "  is not selected for 'DO you have undergone treatment' ");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit(); 
			}

		}
		
		
	}
	
	
	public static void verifyErrorMessage(WebDriver driver, String fieldname, ExtentTest test) throws IOException {	
		
		//span.help-block.error-block
		//driver.findElement(By.xpath("//span[contains(@class,'middle') and contains(text(), 'Next')]"))
		//String errorMessage;
		
		String errorMessage;
		switch (fieldname.trim()) {
		  case "Given Name":
			
			errorMessage = "cannot be blank or exceed 100 chars.";
			WebElement errMsgObj = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}		
			
		    break;
		  case "Email":
			errorMessage = "can only contain a valid email address and cannot exceed 100 chars.";
			WebElement errMsgObj1 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj1.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
			break;
		  case "Surname":
			errorMessage = "cannot be blank or exceed 100 chars.";
			WebElement errMsgObj2 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj2.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
			break;
		  case "Password":
			errorMessage = "must contain at least 8 characters";
			WebElement errMsgObj3 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj3.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
			break;
		  case "Date of Birth":
			errorMessage = "must be a iso datetime.";
			WebElement errMsgObj4 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj4.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
		    break;
		  case "Confirm Password":
			errorMessage = "must contain at least 8 characters";
			WebElement errMsgObj5 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj5.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
			break;
		  case "Phone":
			errorMessage = "can only contain a valid phone number.";
			WebElement errMsgObj6 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj6.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}	
			break;
		  case "Residential Address":
			errorMessage = "cannot be blank.";
			WebElement errMsgObj7 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj7.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname );
			}	
			break;
		  case "Gender":
			errorMessage = "must select an option";
			WebElement errMsgObj8 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj8.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname );
			}	
		    break;
		  case "Postcode":
			errorMessage = "can only contain 6 digits.";
			WebElement errMsgObj9 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj9.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Race":
			errorMessage = "must select an option";
			WebElement errMsgObj10 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj10.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Emergency Contact Name":
			errorMessage = "cannot be blank or exceed 100 chars.";
			WebElement errMsgObj11 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj11.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break; 
		  case "Emergency Contact Number":
			errorMessage = "can only contain a valid phone number.";
			WebElement errMsgObj12 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj12.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Last 4 characters of NRIC number":
			errorMessage = "should be 4 chars.";
			WebElement errMsgObj13 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj13.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Is Legally Authorised":
			errorMessage = "must select an option";
			WebElement errMsgObj14 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj14.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Mobile Device":
			errorMessage = "must select an option";
			WebElement errMsgObj15 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj15.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Fulfillment Type":
			errorMessage = "must choose at least one option";
			WebElement errMsgObj16 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj16.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
		    break;
		  case "Bank Name":
			errorMessage = "cannot be blank or exceed 100 chars.";
			WebElement errMsgObj17 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj17.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Bank Account Number":
			errorMessage = "can only be a number and cannot exceed 20 digits";
			WebElement errMsgObj18 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj18.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Branch Code":
			errorMessage = "cannot be blank or exceed 5 chars";
			WebElement errMsgObj19 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj19.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname );
			}
			break;
		  case "Training Session":
			errorMessage = "cannot be blank.";
			WebElement errMsgObj20 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj20.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
		    break;
		  case "Have Been Convicted":
			errorMessage = "must select an option";
			WebElement errMsgObj21 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj21.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Have Case":
			errorMessage = "must select an option";
			WebElement errMsgObj22 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj22.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
		  case "Have Undergone Treatment":
			errorMessage = "must select an option";
			WebElement errMsgObj23 = driver.findElement(By.xpath("//span[contains(@class,'help-block') and contains(text()," + "'" + errorMessage + "'"  + ")]"));
			
			if(errMsgObj23.isDisplayed()) {

				test.log(Status.PASS, "Step passed. Expected error message : " + "'" + errorMessage + "'"  + " is displayed for field :" + fieldname  );		
			}else {
				test.log(Status.FAIL, "Step FAILED. Expected error message : " + "'" + errorMessage + "'"  + " is Not displayed for field :" + fieldname  );
			}
			break;
			
			
			
	
		}
		
	
		
		
		
		

	}

	

}

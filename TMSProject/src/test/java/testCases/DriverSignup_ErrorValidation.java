package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utilities.MyUtilities;

public class DriverSignup_ErrorValidation extends MyUtilities {
	
	XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFCell cell;
    String testSite = "https://dp-test.urbanfox.asia/delivery-person/signup";
    String driverLoc = "/usr/bin/chromedriver";
    String testDataLoc = "/Test_Data_TMS.xlsx";
    
    
    @Test(priority=0)
    public void AllFIeldsBlank () throws IOException{
    	
    	ExtentTest test = extent.createTest("TESTCASE XX : Verify error message displayed when all fields are left blank.");
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		System.out.println(fileLocation);	
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		//get the sheet name "DriverSignUp-Sucess"
		sheet= workbook.getSheet("DriverSignUp-Sucess");
		//enable this to execute from jenkins
		System.setProperty("webdriver.chrome.driver",driverLoc);
    	
    	System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		myWait(3000);
		
		driver.get(testSite);
		
		//click "I declare"
		WebElement iDeclare = driver.findElement(By.xpath("//div[2]/div/div/fieldset/div[1]/div/label"));
		iDeclare.click();
		
		myWait(3000);
		//click "I Agree"
		WebElement iAgree = driver.findElement(By.xpath("//div[@class='checkbox']//label//div"));	
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", iAgree);
		
		myWait(2000);
		//The "Submit" will be enabled once i declare and I agree checkboxes were clicked
		WebElement iSubmit = driver.findElement(By.xpath("//div/form/div[7]/button"));
		if(iSubmit.isEnabled()) {
			iSubmit.click();

		}
    	
    	
    }
	
	
	
	
	

}

package utilities;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;



public class MyUtilities {
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	
	/*declare variables to be used for the entire test
	 * these are variables for user acct, and driver acct
	*/
	public final String MLNumber = "ML5225607";
	public final String username = "ronwald.sandoval@urbanfox.asia";
	public final String password = "DpStagPassword1*";
	public final String driverUID = "675814";
	public final String driverUsername = "ronwald_sandoval@yahoo.com";
	public final String driverPassword = "DpTestPassword1*";
	
	public final String testSite = "https://dp-test.urbanfox.asia/";
	public final String driverLoc = "/usr/bin/chromedriver";
	public final String testDataLoc = "/Test_Data_TMS.xlsx";
	public final String deliveryRecipient = "Bean";
	
	
	
	static WebDriver driver;
	
	
	@Parameters({ "OS", "browser" })
	@BeforeTest
	public void startReport(String OS, String browser) {
		
		//initialize html report
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/test-output/testReport.html");
		//initialize extent reports and attach the html reporter
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		
		 //To add system or environment info by using the setSystemInfo method.
        extent.setSystemInfo("OS", OS);
        extent.setSystemInfo("Browser", browser);
        	
		//configure the look and feel of the report
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setDocumentTitle("Test Automation Report");
		htmlReporter.config().setReportName("Test Report");
			
	}
	
	
	public static void takeScreenshot(WebDriver driver, ExtentTest test) throws IOException {
		
		//driver.manage().window().maximize();
		//File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(scrFile, new File("C:\\Workspace\\WebDriverTest\\test-output\\screenshot.png"));
		//test.addScreenCaptureFromPath("C:\\Workspace\\WebDriverTest\\test-output\\screenshot.png");
		
		
		String screenShot = System.getProperty("user.dir")+"\\Artifacts\\FileName.png";
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(screenShot));
			
	}
	
	@AfterTest
    public void tearDown() {
    	//to write or update test information to reporter
        extent.flush();
    }
	
	
	public static WebDriver testInitialize () {
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver_win32/chromedriver.exe");
		WebDriver driver = new ChromeDriver();		
		return driver;
		
	}
	
	public void waitForElement(WebDriver driver, WebElement element) {
	    WebDriverWait wait = new WebDriverWait(driver,5);
	    wait.until(ExpectedConditions.visibilityOf(element));

	}
	
	public static void myWait(long wait) {
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static  AppiumDriver<MobileElement> testWebMobileInitialize ()  {
		  
	  	AppiumDriver<MobileElement> driver = null;
	  	
	  	//set desired capabilities
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "My Emulator");
		cap.setCapability("udid", "emulator-5554");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "9");
		cap.setCapability("browserName", "Chrome");
		cap.setCapability("noReset", true);
							
		//set chrome driver location
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver_win32/chromedriver.exe");
					
		//instantiate appium driver
		
		try {
			driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
			
		}catch(MalformedURLException e) {
			System.out.println(e.getMessage());
			
		}
					
		return driver;
	
	}
	
	
	public static AppiumDriver<MobileElement> setupNativeMobile() {
		
		
		AppiumDriver<MobileElement> driver = null;
	
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("BROWSER_NAME", "Android");
		cap.setCapability("VERSION", "28");
		cap.setCapability("deviceName", "Pixel_2_API_30");
		cap.setCapability("platformName", "Android");
		cap.setCapability("autoGrantPermissions", true);
		cap.setCapability("appPackage", "asia.urbanfox.deliver");
		cap.setCapability("appActivity", "asia.urbanfox.deliver.MainActivity");
		
		//cap.setCapability("appPackage", "com.android.calculator2");
		//cap.setCapability("appActivity", "com.android.calculator2.Calculator");
		
		//driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		try {
			driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
					
		return driver;
			
	}
	
	public static ChromeOptions setupChromeOptions() {
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		System.setProperty("webdriver.chrome.args", "--disable-logging");
		System.setProperty("webdriver.chrome.silentOutput", "true");
		options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
		options.addArguments("disable-infobars"); // disabling infobars
		options.addArguments("--disable-extensions"); // disabling extensions
		options.addArguments("--disable-gpu"); // applicable to windows os only
		options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
		options.addArguments("window-size=1024,768"); // Bypass OS security model
		options.addArguments("--log-level=3"); // set log level
		options.addArguments("--silent");//
		options.addArguments("--verbose");//
		options.addArguments("--whitelisted-ips=");
		
		return options;
		
	}
	
	public static ChromeOptions setupChromeOptionsLocal() {
		
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
		
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		//options.addArguments("start-maximized");
		
	
		return options;
		
	}
	
	
	public static boolean isValidDate(String inDate) {
        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:ms");
        SimpleDateFormat mydateFormat = new SimpleDateFormat("dd/mm/yyyy");
        mydateFormat.setLenient(false);
        try {
        	mydateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
	
	public void switchTab(WebDriver driver){
		
		//The following snippet of code is for switching to new tab
		Set<String> handles = driver.getWindowHandles();
		Iterator<String> it = handles.iterator();
		   while (it.hasNext()){
		   String child = it.next();
		   driver.switchTo().window(child);
		   }
		
		
	}
	
	
	

	

}

package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import pageObjects.Deliver_Home;
import utilities.MyUtilities;

public class ClientRegistration extends MyUtilities {
	
	
	
	XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFCell cell;
    String testSite = "https://dp-test.urbanfox.asia/p/public/tracking?t_id=";
    String driverLoc = "/usr/bin/chromedriver";
    String testDataLoc = "/Test_Data_TMS.xlsx";
	


	@Test(priority = 0)
	public void testCase1() throws IOException {
		
		
			ExtentTest test = extent.createTest("TESTCASE 1 : Test Delivery Tracking Page Test 1");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("3");
		
			
			System.setProperty("webdriver.chrome.driver",driverLoc);

			WebDriver driver = new ChromeDriver(setupChromeOptions());
			driver.get(testSite);
			myWait(3000);
			
			
			
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
								
				cell = sheet.getRow(i).getCell(0);
				/**we need to check if the cell is empty or not
				For some reason, XSSF is reading previosly updated cell*/
				if(cell==null) {
					driver.quit();
				}else {
					cell.setCellType(CellType.STRING);
					
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
			
				}
				

			}

		
	}
	
	
	@Test(priority = 1)
	public void testCase2() throws IOException {
		
		
			ExtentTest test = extent.createTest("TESTCASE 2 : Test Delivery Tracking Page Test 2");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			System.setProperty("webdriver.chrome.driver",driverLoc);

			WebDriver driver = new ChromeDriver(setupChromeOptions());
			driver.get(testSite);
			myWait(3000);
			
			
			
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
								
				cell = sheet.getRow(i).getCell(0);
				/**we need to check if the cell is empty or not
				For some reason, XSSF is reading previosly updated cell*/
				if(cell==null) {
					driver.quit();
				}else {
					cell.setCellType(CellType.STRING);
					
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
			
				}
				

			}

		
	}
	
	
}

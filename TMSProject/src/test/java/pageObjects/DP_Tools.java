package pageObjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import utilities.MyUtilities;

public class DP_Tools {
	
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	private static WebElement element = null;
	
	
	
	

	
	
	public static WebElement ClickLink (WebDriver driver, String linkname) {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		element = driver.findElement(By.linkText(linkname));	
		return element;
		
	}
	
	public static void EnterText (WebDriver driver, String classname, String textToSearch, ExtentTest test) throws IOException {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		//element = driver.findElement(By.className(classname));	
		List<WebElement> elements = driver.findElements(By.className(classname));	
		int size = elements.size();
		
		
		if(size>0) {
	    	elements.get(0).sendKeys(textToSearch);
	    	test.log(Status.PASS, "Step passed. Text : " + textToSearch + " is successfully entered");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Text : " + textToSearch + " is not successfully entered. Closing this test.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
	
		
	}
	
	
	public static void ClickButton (WebDriver driver, String buttonName, ExtentTest test) throws IOException {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		//element = driver.findElement(By.xpath("//span/button[text()='"+ buttonName + "']"));
		List<WebElement> elements = driver.findElements(By.xpath("//span/button[text()='"+ buttonName + "']"));
		
		int size = elements.size();
		
		if(size>0) {
			test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked");
	    	elements.get(0).click();
	    	
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	driver.quit();
	    	   	
	    }
		
		
	}
	
	
	
	//this method is used to click Caret
	public static void clickCaret(WebDriver driver, String buttonName, ExtentTest test) throws IOException {
		
	    List<WebElement> elements = driver.findElements(By.xpath("//span/a[text()='"+ buttonName + "']"));
	    System.out.println("trying to click caret" + " " +  elements.size());
	    
	    driver.manage().window().maximize();
	    MyUtilities.takeScreenshot(driver, test);
	    
	    if(elements.size()>0) {	
	    	MyUtilities.takeScreenshot(driver, test);
    		elements.get(0).click();
	    	System.out.println("caret clicked");
	    	test.log(Status.PASS, "Step passed. Caret : " + buttonName + " is clicked");
		    		
	    }else {
	    	test.log(Status.FAIL, "Step failed. Caret : " + buttonName + " is not clicked. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test);
	    	System.out.println("caret not clicked");

	    	   	
	    }
	    
	}
	
	//this method can be used to switch to a tab, but can be used only once
	public static void switchTab(WebDriver driver ) {
		
		String currentTabHandle = driver.getWindowHandle();
		String newTabHandle = driver.getWindowHandles()
		       .stream()
		       .filter(handle -> !handle.equals(currentTabHandle ))
		       .findFirst()
		       .get();
		driver.switchTo().window(newTabHandle);
		
	}
	
	
	//this method can be used to switch to a specific tab, but the tab name must be provided
	public static void switchSpecificTab(WebDriver driver, String tabName) {
		String your_title = tabName;
		String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
		for(String winHandle : driver.getWindowHandles()){
		   if (driver.switchTo().window(winHandle).getTitle().equals(your_title)) {
		     //This is the one you're looking for
		     break;
		   } 
		   else {
		      driver.switchTo().window(currentWindow);
		   } 
		}
	}
	

}

package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import pageObjects.DP_ControllerReport;
import pageObjects.DP_Home;
import pageObjects.DP_Login;
import pageObjects.DP_Tools;
import utilities.MyUtilities;

/* Test Case Author : Ronwald Sandoval
 * 
 * This test case will process a delivery order in dp-test.
 * First it will run the TC01, where the DO will be "Order Processing"
 * Then, TC02 will create a bundle and assign to a driver
 * Then, TC03 will update the DO to "Departed Hub"
 * */

 //675814 - driver id
public class WareHouse_Delivery extends MyUtilities {
	
	XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFCell cell;
	
	
	
	
	
	

	
	@Test(priority = 1)
	public void createBundle() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE 02 : Test Successful Creation of Bundle and Assign to Driver for a delivery order");
		

		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		sheet= workbook.getSheet("MLList");
		

		for (int i = 1;i<=sheet.getLastRowNum();i++){
			
			cell = sheet.getRow(i).getCell(0);
				
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					System.out.println("No more data. Need to stop the test");
					//driver.quit();		
					System.out.println(regData);
				}
				
				break;	
			}else {
				
				//disable this if testing from jenkins, enable when testing from local
				
				WebDriver driver = new ChromeDriver(setupChromeOptionsLocal());
				driver.manage().window().setSize(new Dimension(1440, 900));
				
				
				//Enable this when testing via jenkins, disable when testing from local
				//System.setProperty("webdriver.chrome.driver",driverLoc);
				//WebDriver driver1 = new ChromeDriver(setupChromeOptions());
				
				driver.get(testSite);
				driver.manage().window().maximize();
				myWait(2000);
				
						
				//login to the test site
				DP_Login.LoginButton(driver).click();
				DP_Login.TextField(driver, "username").sendKeys(username);
				DP_Login.TextField(driver, "password").sendKeys(password);
				DP_Login.SignIn(driver).click();
				test.log(Status.PASS, "User is successfully logged in to DP page");
				
				cell.setCellType(CellType.STRING);
				
				String doNumber = cell.getStringCellValue();
				System.out.println("inside the else " + " " + doNumber);
				
				myWait(2000);
		        DP_Home.ClickLink(driver, "Tools", test);
		        myWait(2000);
		        DP_Home.ClickLink(driver, "Transaction Search", test);
		        myWait(2000);
				DP_Tools.EnterText(driver, "form-control", doNumber, test);
				myWait(2000);
				DP_Tools.ClickButton(driver, "Search", test);
				myWait(3000);	
				driver.manage().window().maximize();
				myWait(3000);
				DP_Tools.clickCaret(driver, "^", test);
				
			
			    //The ML details are displayed on a new tab, so we need to switch the control
				DP_Tools.switchTab(driver);
				myWait(2000);

				
				//verify ML page is displayed. If displayed, pass the test, else fail the test
				if(driver.getTitle().contains("ML")) {
				    //Pass
					test.log(Status.PASS, driver.getTitle());
				    test.log(Status.PASS, "Warehouse Delivery orer details are displayed");
				}    
				
				else {
				    //Fail
					test.log(Status.FAIL, driver.getTitle());
					test.log(Status.FAIL, "Warehouse Delivery orer details are Not displayed");
					//take screenshot if fail so its easier to investigate
					takeScreenshot(driver, test);
					
				}
				myWait(1000);
				
				DP_Home.ClickLink(driver, "bundle, schedule & more", test);

				
				DP_Tools.switchSpecificTab(driver, "Controller Report");

				myWait(1000);
				DP_ControllerReport.clickCheckBox(driver, test);
				DP_ControllerReport.clickButton(driver, "Create", test);
				DP_ControllerReport.clickLinkText(driver, "Bundle", test);
				myWait(1000);
				DP_ControllerReport.enterText(driver, "1", test);
				myWait(1000);
				DP_ControllerReport.clickButton(driver, "Next", test);
				myWait(1000);
				
				driver.findElement(By.cssSelector("div > div > input")).sendKeys(driverUID);
				myWait(1000);
				DP_ControllerReport.clickButton(driver, "Next", test);
				myWait(1000);
				DP_ControllerReport.clickButton(driver, "Done", test);
				myWait(2000);
				
				
				//verify bundle creation confirmation page is displayed
				String expMessage = "has been created.";
				String actualMessage = driver.findElement(By.xpath("//div[@class='modal-body']/div")).getText();
				

				System.out.println(actualMessage);
				
				if(actualMessage.trim().contains(expMessage.trim())) {
					//Pass
				    test.log(Status.PASS, "Bundle is created for the delivery order");
				    test.log(Status.INFO, actualMessage + " - Test ends here.");
					
				}else {
					//Fail
					test.log(Status.FAIL, "Bundle is not created for the delivery order");
					takeScreenshot(driver, test);
					
				}
				
				DP_ControllerReport.clickButton(driver, "Close", test);
				myWait(1000);
				driver.quit();
				
			}
			
		}
	
	
		//close the driver
		//driver.quit();
		
	}
	
	
	@Test(priority = 0)
	public void orderProcessing() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE 01 : Test Successful Scan Transaction to Order Processing");
		
		//set the chrome driver, launch Chrome browser and go to test site,  and maximize the window
		//disable this if testing from jenkins
		//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		
		WebDriver driver = new ChromeDriver(setupChromeOptionsLocal());
		driver.manage().window().setSize(new Dimension(1440, 900));
		
		
		//Enable this when testing via jenkins
		//System.setProperty("webdriver.chrome.driver",driverLoc);
		//WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);
		myWait(3000);
		
		
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		sheet= workbook.getSheet("MLList");
		

		driver.manage().window().maximize();
		//login to the test site
		DP_Login.LoginButton(driver).click();
		DP_Login.TextField(driver, "username").sendKeys(username);
		DP_Login.TextField(driver, "password").sendKeys(password);
		DP_Login.SignIn(driver).click();
		test.log(Status.PASS, "User is successfully logged in to DP page");
		
	
		for (int i = 1;i<=sheet.getLastRowNum();i++){
			
			cell = sheet.getRow(i).getCell(0);
			
			
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					//System.out.println("No more data. Need to stop the test");
					driver.quit();			
				}
				
				break;	
			}else {
				
				cell.setCellType(CellType.STRING);
				
				String doNumber = cell.getStringCellValue();
				
				myWait(1000);
		        DP_Home.ClickLink(driver, "Tools", test);
		        myWait(1000);
		        DP_Home.ClickLink(driver, "Scan Transaction To Order Processing", test);
		        myWait(1000);
				DP_Tools.EnterText(driver, "form-control", doNumber, test);
				myWait(1000);
				DP_Tools.ClickButton(driver, "Scan Order Processing", test);
				myWait(3000);	
				
				//verify bundle creation confirmation page is displayed
				String expMessage = "Order Processing";
				String actualMessage = driver.findElement(By.xpath("//h3")).getText();
				
				System.out.println(actualMessage);
				
				if(actualMessage.trim().contains(expMessage.trim())) {
					//Pass
				    test.log(Status.PASS, "Delivery order is Successfully Scanned to Order Processing");
				    test.log(Status.INFO, actualMessage + " - Test ends here.");
					
				}else {
					//Fail
					test.log(Status.FAIL, "Delivery order is Not Successfully Scanned to Order Processing");
					takeScreenshot(driver, test);
					
				}
				
			}
			
				
		}
		
		
		//close the driver
		driver.quit();
		
	}
	
	
	@Test(priority = 2)
	public void departedHub() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE 03 : Test Successful Scan Transaction to Departed Hub");
		
		//set the chrome driver, launch Chrome browser and go to test site,  and maximize the window
		//disable this if testing from jenkins
		//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		
		WebDriver driver = new ChromeDriver(setupChromeOptionsLocal());
		driver.manage().window().setSize(new Dimension(1440, 900));
		
		
		//use this when testing via jenkins
		//System.setProperty("webdriver.chrome.driver",driverLoc);
		//WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);
		myWait(3000);
		
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		sheet= workbook.getSheet("MLList");
		
		driver.manage().window().maximize();
		
		//login to the test site
		MyUtilities.takeScreenshot(driver, test);
		DP_Login.LoginButton(driver).click();
		MyUtilities.takeScreenshot(driver, test);
		DP_Login.TextField(driver, "username").sendKeys(driverUsername);
		MyUtilities.takeScreenshot(driver, test);
		DP_Login.TextField(driver, "password").sendKeys(driverPassword);
		MyUtilities.takeScreenshot(driver, test);
		DP_Login.SignIn(driver).click();
		test.log(Status.PASS, "User is successfully logged in to DP page");
		
		for (int i = 1;i<=sheet.getLastRowNum();i++){
			
			cell = sheet.getRow(i).getCell(0);	
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					//System.out.println("No more data. Need to stop the test");
					driver.quit();			
				}
				
				break;
			
		}else{
			cell.setCellType(CellType.STRING);
			String doNumber = cell.getStringCellValue();
			
			myWait(2000);
			MyUtilities.takeScreenshot(driver, test);
	        DP_Home.ClickLink(driver, "Tools", test);
	        myWait(2000);
	        DP_Home.ClickLink(driver, "Scan Transaction To Departed Hub", test);
	        myWait(2000);
			DP_Tools.EnterText(driver, "form-control", doNumber, test);
			myWait(2000);
			DP_Tools.ClickButton(driver, "Scan Departed Hub", test);
			myWait(3000);	
			
			//verify bundle creation confirmation page is displayed
			String expMessage = "Departed Hub";
			String actualMessage = driver.findElement(By.xpath("//h3")).getText();
			
			
			if(actualMessage.trim().contains(expMessage.trim())) {
				//Pass
			    test.log(Status.PASS, "Delivery order is Successfully Scanned to Departed Hub");
			    test.log(Status.INFO, actualMessage);
				
			}else {
				//Fail
				test.log(Status.FAIL, "Delivery order is Not Successfully Scanned to Departed Hub");
				takeScreenshot(driver, test);
				
			}
			
			
		}
			
		}
		
			
		//close the driver
		driver.quit();
		
	}
		
	
	
	
	

}

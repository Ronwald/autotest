package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.MyUtilities;

public class DP_Login {
	
	private static WebElement element = null;
	
	
	public static WebElement LoginButton (WebDriver driver) {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		element = driver.findElement(By.linkText("Log in"));	
		
		return element;
		
	}
	// this can be used for entering username and password
	public static WebElement TextField (WebDriver driver, String fieldName) {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		element = driver.findElement(By.name(fieldName));	
		return element;
		
	}
	
	
	public static WebElement SignIn (WebDriver driver) {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		element = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/form/div[3]/button"));	
		return element;
		
	}
	

}

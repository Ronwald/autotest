package pageObjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import utilities.MyUtilities;

public class MobileHome {
	
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	//private static WebElement element = null;
	
	
	public static void EnterText (WebDriver driver, String type, String textToSearch, ExtentTest test) throws IOException {	

		//driver.findElement(MobileBy.xpath("//android.widget.EditText[contains(@password,'false')]")).sendKeys("ronwald.sandoval@urbanfox.asia");
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget.EditText[contains(@password,'" + type + "')]"));	
		int size = elements.size();

			
		if(size>0) {
	    	elements.get(0).sendKeys(textToSearch);
	    	test.log(Status.PASS, "Step passed. Text : " + textToSearch + " is successfully entered");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Text : " + textToSearch + " is not successfully entered. Closing this test.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
	
		
	}
	
	
	public static void ClickImage (WebDriver driver, String type, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Image[contains(@clickable,'false')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget.Image[contains(@clickable,'" + type + "')]"));	
		int size = elements.size();
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Urbanfox logo is successfully clicked (to make the login button visible).");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Urbanfox logo is not clicked (to make the login button visible).");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
	
		
	}
	
	public static void ClickButton (WebDriver driver, String type, String buttonName, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget.Button[contains(@clickable,'" + type + "')]"));	
		int size = elements.size();
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is successfully clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	
	public static void ClickTextViewViaIndex(WebDriver driver, String myType, int index, String buttonName, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
		//driver.findElement(MobileBy.xpath("//android.widget.TextView[contains(@index,'0')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget." + myType  + "[contains(@index,'" + index + "')]"));	
		int size = elements.size();
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	public static void ClickTextViewViaText(WebDriver driver, String myType, String myText, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
		//driver.findElement(MobileBy.xpath("//android.widget.TextView[contains(@index,'0')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget." + myType  + "[contains(@text,'" + myText + "')]"));	
		int size = elements.size();
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + myText + " is clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + myText + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	
	public static void ClickAndroidViewViaIndex(WebDriver driver, String myType, int index, String buttonName, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
		//driver.findElement(MobileBy.xpath("//android.widget.TextView[contains(@index,'0')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.view." + myType  + "[contains(@index,'" + index + "')]"));	
		int size = elements.size();
		System.out.println(size);
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	
	public static void ClickTextViewViaBounds(WebDriver driver, String bounds, String buttonName, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
		//driver.findElement(MobileBy.xpath("//android.widget.TextView[contains(@index,'0')]")).click();
		
		//[77,1696][140,1761]
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget.TextView[contains(@bounds,'" + bounds + "')]"));	
		int size = elements.size();
		System.out.println(size);
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	public static void ClickButtonByText (WebDriver driver, String buttonName, ExtentTest test) throws IOException {	
		
		//driver.findElement(MobileBy.xpath("//android.widget.Button[contains(@clickable,'true')]")).click();
	
		List<WebElement> elements = driver.findElements(MobileBy.xpath("//android.widget.Button[contains(@text,'" + buttonName + "')]"));	
		int size = elements.size();
			
		if(size>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is successfully clicked.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked.");   	
	    	MyUtilities.takeScreenshot(driver, test);    	
	    	driver.quit();
	    }
		
	}
	
	public static void scrollDown(WebDriver driver) {

	    //The viewing size of the device
	    Dimension size = driver.manage().window().getSize();

	    //Starting y location set to 80% of the height (near bottom)
	    int starty = (int) (size.height * 0.70);
	    //Ending y location set to 20% of the height (near top)
	    int endy = (int) (size.height * 0.20);
	    //x position set to mid-screen horizontally
	    int startx = (int) size.width / 2;

	    scroll(driver, startx, starty, startx, endy);

	}


	private static void scroll(WebDriver driver, int startx, int starty, int endx, int endy) {
		
		//TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
		AndroidTouchAction touch = new AndroidTouchAction ((PerformsTouchActions) driver);
		
		//touch.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, endy)).release().perform();

		touch.press(PointOption.point(startx, starty))
	               .moveTo(PointOption.point(endx, endy))
	              .release()
	              .perform();
		// TODO Auto-generated method stub
		
	}
	
	public static void myScroll(WebDriver driver) {
		
		//TouchAction touchAction = new TouchAction((PerformsTouchActions) driver);
		AndroidTouchAction touch = new AndroidTouchAction ((PerformsTouchActions) driver);
		
		//touch.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, endy)).release().perform();

		touch.press(PointOption.point(0, 800))
	               .moveTo(PointOption.point(0, 400))
	              .release()
	              .perform();
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	

}

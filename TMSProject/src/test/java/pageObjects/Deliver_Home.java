package pageObjects;

import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;




import utilities.MyUtilities;

public class Deliver_Home {
	
	
private static WebElement element = null;
private static XSSFRow row;
private static XSSFSheet excelWSheet;


	
	
	public static void ClickLinkText (WebDriver driver, String linkText, ExtentTest test) throws IOException {	
			List<WebElement> elements = driver.findElements(By.linkText(linkText));	
			int size = elements.size();
			
			if(size>0) {
				test.log(Status.PASS, "Step passed. Button : " + linkText + " is clicked");
		    	elements.get(0).click(); 	
		    }else {
		    	test.log(Status.FAIL, "Step failed. Button : " + linkText + " is not clicked. Closing this test.");
		    	MyUtilities.takeScreenshot(driver, test); 
		    	driver.quit();    	   	
		    }		
	}
	
	public static void EnterText (WebDriver driver, String fieldname, String valueToEnter, ExtentTest test) throws IOException {	
		//List<WebElement> elements = driver.findElements(By.linkText(linkText));	
		
		List<WebElement> elements = driver.findElements(By.name(fieldname));
		
		int size = elements.size();
		
		
		if(size>0) {
			test.log(Status.PASS, "Step passed. Value : " + valueToEnter + " is entered");
	    	elements.get(0).sendKeys(valueToEnter);	
	    }else {
	    	test.log(Status.FAIL, "Step failed. Value : " + valueToEnter + " is not entered. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	//driver.quit();    	   	
	    }		

	}
	
	public static XSSFRow getRowData(int RowNum) {
        try {
            row = excelWSheet.getRow(RowNum);
            return row;
        } catch (Exception e) {
            throw (e);
        }
    }
	
	public static void EnterTextByClassName (WebDriver driver, String className, String valueToEnter, ExtentTest test) throws IOException {	
		//List<WebElement> elements = driver.findElements(By.linkText(linkText));	
		
		List<WebElement> elements = driver.findElements(By.className(className));
		
		int size = elements.size();
		
		System.out.println(size);
				
		if(size>0) {
			test.log(Status.PASS, "Step passed. Value : " + valueToEnter + " is entered");
	    	elements.get(0).sendKeys(valueToEnter);	
	    }else {
	    	test.log(Status.FAIL, "Step failed. Value : " + valueToEnter + " is not entered. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	//driver.quit();    	   	
	    }		

	}
	
	public static void ClickButton (WebDriver driver, String buttonName, ExtentTest test) throws IOException {	
		//element = driver.findElement(By.xpath("//*[@id=\"banner-container\"]/nav/div/div[2]/ul/li[1]/a"));	
		//element = driver.findElement(By.xpath("//span/button[text()='"+ buttonName + "']"));
		List<WebElement> elements = driver.findElements(By.xpath("//span/button[text()='"+ buttonName + "']"));
		
		int size = elements.size();
		
		if(size>0) {
			test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked");
	    	elements.get(0).click();
	    	
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	driver.quit();
	    	   	
	    }
		
		
	}
	
	
	public static void ClearText (WebDriver driver, String className, ExtentTest test) throws IOException {	
		//List<WebElement> elements = driver.findElements(By.linkText(linkText));	
		
		List<WebElement> elements = driver.findElements(By.className(className));
		
		int size = elements.size();
		
				
		if(size>0) {
			test.log(Status.PASS, "Step passed. Value is cleared");
	    	elements.get(0).clear();
	    }else {
	    	test.log(Status.FAIL, "Step failed. Value is not cleared. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	//driver.quit();    	   	
	    }		

	}
	
	
	public static void verifyElementPresent (WebDriver driver, String cssSelector, ExtentTest test) throws IOException {	
		//List<WebElement> elements = driver.findElements(By.linkText(linkText));	
		
		List<WebElement> elements = driver.findElements(By.cssSelector(cssSelector));
		
		int size = elements.size();
		
				
		if(size>0) {
			test.log(Status.PASS, "Step passed. Object is displayed. " + elements.get(0).getText() );

	    }else {
	    	test.log(Status.FAIL, "Step failed. Object is not displayed. Closing this test.");
	    	MyUtilities.takeScreenshot(driver, test); 
	    	driver.quit();    	   	
	    }		

	}
	
	
	
	
	
	
	







}

package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import pageObjects.DP_DriverSignup;
import utilities.MyUtilities;

public class DriverSignUp extends MyUtilities {
	
	XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFCell cell;

    
    
    
    @Test(priority=0)
	public void successFulRegistration() throws IOException {
		
		
		ExtentTest test = extent.createTest("TESTCASE 1 : Test successful driver registration by completing all required fields.");
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		System.out.println(fileLocation);	
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		//get the sheet name "DriverSignUp-Sucess"
		sheet= workbook.getSheet("DriverSignUp-Sucess");
		//enable this to execute from jenkins
		System.setProperty("webdriver.chrome.driver",driverLoc);
		

		WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);
		
		
	
		//loop through the data
		for(int i = 1;i<=sheet.getLastRowNum();i++) {
			//disable this if running from jenkins
			//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
			//WebDriver driver = new ChromeDriver();
			myWait(3000);
			
			cell = sheet.getRow(i).getCell(0);
		
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					//System.out.println("No more data. Need to stop the test");
					driver.quit();			
				}
				
				break;
		
			}else {
				
				System.out.println(i);
				System.out.println(cell);
				
				cell.setCellType(CellType.STRING);
				//get data from excel
				String regData = cell.getStringCellValue();		
				driver.get(testSite);
				//build the field names
				String myStringbuild = new StringBuilder()
						.append("given_name;")
						.append("email;")
						.append("surname;")
						.append("password[password];")
						.append("birthday;")
						.append("password[confirm_password];")
						.append("phone;")
						.append("address;")
						.append("gender;")
						.append("postcode;")
						.append("race;")
						.append("emergency_contact_name;")
						.append("emergency_contact_number;")
						.append("identification_number;")
						.append("authorised;")
						.append("smartphone;")
						.append("fulfillDelivery;")
						.append("bankName;")
						.append("account_number;")
						.append("branch_code;")
						.append("training;")
						.append("isConvicted;")
						.append("haveCase;")
						.append("haveTreatment;")
						.toString();

				//create arrays for the data, and the fieldnames
				String[] valueArray;
				String[] fieldArray;
				String delimiter = ";";
				
				valueArray = regData.split(delimiter);
				fieldArray = myStringbuild.split(delimiter);
				
				for (int x = 0; x < valueArray.length; x++) {
					
						String fieldName = fieldArray[x];
						String myData = valueArray[x];
						myWait(1000);
						
						
						if(fieldName.equals("birthday")) {
							DP_DriverSignup.SelectBirthDate(driver, 3, "Date of Birth", myData, test);			
						}
						else if(fieldName.equals("training")){
							//DP_DriverSignup.SelectTrainingDate(driver, 13, "Date of Training Session", myData, test);			
						}
						else if(fieldName.equals("country")){
							//currently no need for this, as the country will default to Singapore			
						}				
						else if(fieldName.equals("gender")){
							DP_DriverSignup.clickRadioButtonGender(driver, myData, test);		
						}
						else if(fieldName.equals("race")){
							DP_DriverSignup.clickRadioButtonRace(driver, myData, test);	
						}
						else if(fieldName.equals("authorised")){
							DP_DriverSignup.clickRadioButtonCitizenship(driver, myData, test);	
						}
						else if(fieldName.equals("smartphone")){
							DP_DriverSignup.clickRadioButtonMobileType(driver, myData, test);	
						}
						else if(fieldName.equals("fulfillDelivery")){	
							DP_DriverSignup.selectFulfill(driver, myData, test);
						}
						else if(fieldName.equals("bankName")){
							DP_DriverSignup.selectBankName(driver, myData, test);
						}
						else if(fieldName.equals("isConvicted")){
							DP_DriverSignup.clickRadioButtonConvicted(driver, myData, test);
						}
						else if(fieldName.equals("haveCase")){
							DP_DriverSignup.clickRadioButtonCriminal(driver, myData, test);
						}
						else if(fieldName.equals("haveTreatment")){
							DP_DriverSignup.clickRadioButtonTreatment(driver, myData, test);
						}
						else {
							DP_DriverSignup.EnterText(driver, fieldName, myData, test);
						}			
				}
				
				//click "I declare"
				WebElement iDeclare = driver.findElement(By.xpath("//div[2]/div/div/fieldset/div[1]/div/label"));
				iDeclare.click();
				
				myWait(3000);
				//click "I Agree"
				//WebElement iAgree = driver.findElement(By.xpath("//div[6]/div[2]/div/div/fieldset/div[2]/div"));
				WebElement iAgree = driver.findElement(By.xpath("//div[@class='checkbox']//label//div"));
				//WebElement iAgree = driver.findElement(By.cssSelector(".form-group:nth-child(2) > .checkbox > label"));
				//WebElement iAgree = driver.findElement(By.name("agree_tnc"));
				//iAgree.click();
				
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", iAgree);
				
				myWait(2000);
				//The "Submit" will be enabled once i declare and I agree checkboxes were clicked
				WebElement iSubmit = driver.findElement(By.xpath("//div/form/div[7]/button"));
				if(iSubmit.isEnabled()) {
					iSubmit.click();

				}
				
				myWait(1000);
				

				
				//verify that the registration is complete by checking the confirmation message		
				//WebElement iWelcomeOnboard  = driver.findElement(By.xpath("//*[@id=\"react-container\"]/div/div[2]/div"));
				WebElement iWelcomeOnboard  = driver.findElement(By.xpath("//div[@class='signup-success-info']"));
				if(iWelcomeOnboard.isDisplayed()) {
					test.log(Status.INFO, "Test for Successful Registration Complete");		
					MyUtilities.takeScreenshot(driver, test); 
					driver.quit();
				}else {
			    	test.log(Status.FAIL, "Test for Successful Registration Complete is Fail");
			    	MyUtilities.takeScreenshot(driver, test); 
			    	   	
			    }	


			}
		
		}
		
	}
	
    @Test(priority=1)
	public void leaveAllRequiredFieldsBlank() throws IOException {

		ExtentTest test = extent.createTest("TESTCASE 2 : This test verifies that error messages will be displayed if user leave required fields blank.");

		//System.setProperty("webdriver.chrome.driver",driverLoc);
		
		//enable this if running from eclipse
		//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.get(testSite);
		
		//enable this if running from jenkins
		WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);


		myWait(3000);
				
		//build the field names
		String myStringbuild = new StringBuilder()
				.append("Given Name;")
				.append("Email;")
				.append("Surname;")
				.append("Password;")
				.append("Date of Birth;")
				.append("Confirm Password;")
				.append("Phone;")
				.append("Residential Address;")
				.append("Gender;")
				.append("Postcode;")
				.append("Race;")
				.append("Emergency Contact Name;")
				.append("Emergency Contact Number;")
				.append("Last 4 characters of NRIC number;")
				.append("Is Legally Authorised;")
				.append("Mobile Device;")
				.append("Fulfillment Type;")
				.append("Bank Name;")
				.append("Bank Account Number;")
				.append("Branch Code;")
				.append("Training Session;")
				.append("Have Been Convicted;")
				.append("Have Case;")
				.append("Have Undergone Treatment;")
				.toString();
		

		String[] fieldArray;
		
		myWait(2000);

		//click "I declare"
		WebElement iDeclare = driver.findElement(By.xpath("//div[2]/div/div/fieldset/div[1]/div/label"));
		iDeclare.click();
		
		myWait(2000);
		//click "I Agree"
		WebElement iAgree = driver.findElement(By.xpath("//div[@class='checkbox']//label//div"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", iAgree);
		
		myWait(2000);
		//The "Submit" will be enabled onec i declare and I agree checkboxes were clicked
		WebElement iSubmit = driver.findElement(By.xpath("//div/form/div[7]/button"));
		if(iSubmit.isEnabled()) {
			iSubmit.click();
		}

		fieldArray = myStringbuild.split(";");
		
		for (int x = 0; x < fieldArray.length; x++) {
			
			String fieldName = fieldArray[x];
			DP_DriverSignup.verifyErrorMessage(driver, fieldName, test);
			
		}
		
		driver.quit();

		
	}
    
    @Test(priority=2)
    public void dontCompleteAllReqFields() throws IOException {
		
		
		ExtentTest test = extent.createTest("TESTCASE 1 : Test unsuccessful registration if all required fields are not complete.");
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		System.out.println(fileLocation);	
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		//get the sheet name "DriverSignUp-Sucess"
		sheet= workbook.getSheet("DriverSignUp-IncompleteFields");
		//enable this to execute from jenkins. Disable if running directly from eclipse
		System.setProperty("webdriver.chrome.driver",driverLoc);
	
		WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);
		
	
		//loop through the data
		for(int i = 1;i<=sheet.getLastRowNum();i++) {
			//disable this if running from jenkins. Enable if running directly from eclipse
			//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
			//WebDriver driver = new ChromeDriver();
			myWait(3000);
			
			cell = sheet.getRow(i).getCell(0);
		
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					//System.out.println("No more data. Need to stop the test");
					driver.quit();			
				}
				
				break;
		
			}else {
				
				System.out.println(i);
				System.out.println(cell);
				
				cell.setCellType(CellType.STRING);
				//get data from excel
				String regData = cell.getStringCellValue();		
				driver.get(testSite);
				//build the field names
				String myStringbuild = new StringBuilder()
						.append("given_name;")
						.append("email;")
						.append("surname;")
						.append("password[password];")
						.append("password[confirm_password];")
						.append("address;")
						.append("gender;")
						.append("postcode;")
						.append("race;")
						.append("emergency_contact_name;")
						.append("emergency_contact_number;")
						.append("identification_number;")
						.append("fulfillDelivery;")
						.append("account_number;")
						.append("branch_code;")
						.append("training;")
						.append("isConvicted;")
						.append("haveCase;")
						.append("haveTreatment;")
						.toString();

				//create arrays for the data, and the fieldnames
				String[] valueArray;
				String[] fieldArray;
				
				String delimiter = ";";
				
				valueArray = regData.split(delimiter);
				fieldArray = myStringbuild.split(delimiter);
				
				for (int x = 0; x < valueArray.length; x++) {
					
						String fieldName = fieldArray[x];
						String myData = valueArray[x];
						myWait(1000);
						
						
				
						if(fieldName.equals("training")){
							//DP_DriverSignup.SelectTrainingDate(driver, 13, "Date of Training Session", myData, test);			
						}
						else if(fieldName.equals("country")){
							//currently no need for this, as the country will default to Singapore			
						}				
						else if(fieldName.equals("gender")){
							DP_DriverSignup.clickRadioButtonGender(driver, myData, test);		
						}
						else if(fieldName.equals("race")){
							DP_DriverSignup.clickRadioButtonRace(driver, myData, test);	
						}
						else if(fieldName.equals("fulfillDelivery")){	
							DP_DriverSignup.selectFulfill(driver, myData, test);
						}
						else if(fieldName.equals("isConvicted")){
							DP_DriverSignup.clickRadioButtonConvicted(driver, myData, test);
						}
						else if(fieldName.equals("haveCase")){
							DP_DriverSignup.clickRadioButtonCriminal(driver, myData, test);
						}
						else if(fieldName.equals("haveTreatment")){
							DP_DriverSignup.clickRadioButtonTreatment(driver, myData, test);
						}
						else {
							DP_DriverSignup.EnterText(driver, fieldName, myData, test);
						}			
				}
				
				//click "I declare"
				WebElement iDeclare = driver.findElement(By.xpath("//div[2]/div/div/fieldset/div[1]/div/label"));
				iDeclare.click();
				
				myWait(3000);
				//click "I Agree"
				WebElement iAgree = driver.findElement(By.xpath("//div[@class='checkbox']//label//div"));

				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", iAgree);
				
				myWait(2000);
				//The "Submit" will be enabled once i declare and I agree checkboxes were clicked
				WebElement iSubmit = driver.findElement(By.xpath("//div/form/div[7]/button"));
				if(iSubmit.isEnabled()) {
					iSubmit.click();

				}
				
				myWait(1000);
				
						
				//verify error msg is displayed
				String[] fieldError;
				String myFieldError = new StringBuilder()
						.append("Date of Birth;")
						.append("Phone;")
						.append("Is Legally Authorised;")
						.append("Mobile Device;")
						.append("Bank Name;")
						.toString();
				
				fieldError = myFieldError.split(";");
				
				for (int x = 0; x < fieldError.length; x++) {			
					String fieldName = fieldError[x];	
					DP_DriverSignup.verifyErrorMessage(driver, fieldName, test);
					
				}
				
		
				driver.quit();	
				


			}
		
		}
		
	}
    
    @Test(priority=3)
    public void completeAllFieldsIncorrectValue() throws IOException {
		
		
		ExtentTest test = extent.createTest("TESTCASE 4 : Verify error message/s are displayed when incorrect values are entered.");
		String projectDir = System.getProperty("user.dir");
		String fileLocation = projectDir + testDataLoc ;
		System.out.println(fileLocation);	
		File src=new File(fileLocation); 
		FileInputStream fis = new FileInputStream(src);
		workbook = new XSSFWorkbook(fis);
		//get the sheet name "DriverSignUp-Sucess"
		sheet= workbook.getSheet("DriverSignUp-IncorrectValues");
		//enable this to execute from jenkins
		System.setProperty("webdriver.chrome.driver",driverLoc);
		
		
		

		WebDriver driver = new ChromeDriver(setupChromeOptions());
		driver.get(testSite);
		
		
	
		//loop through the data
		for(int i = 1;i<=sheet.getLastRowNum();i++) {
			//disable this if running from jenkins
			//System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver.exe");
			//WebDriver driver = new ChromeDriver();
			myWait(3000);
			
			cell = sheet.getRow(i).getCell(0);
		
			if(cell.toString()=="") {
				//check if there is no more data available in the sheet. If none then finish the execution.
				cell.setCellType(CellType.STRING);
				String regData = cell.getStringCellValue();
				if(regData == "") {
					//System.out.println("No more data. Need to stop the test");
					driver.quit();			
				}
				
				break;
		
			}else {
				
				System.out.println(i);
				System.out.println(cell);
				
				cell.setCellType(CellType.STRING);
				//get data from excel
				String regData = cell.getStringCellValue();		
				driver.get(testSite);
				//build the field names
				String myStringbuild = new StringBuilder()
						.append("given_name;")
						.append("email;")
						.append("surname;")
						.append("password[password];")
						.append("birthday;")
						.append("password[confirm_password];")
						.append("phone;")
						.append("address;")
						.append("gender;")
						.append("postcode;")
						.append("race;")
						.append("emergency_contact_name;")
						.append("emergency_contact_number;")
						.append("identification_number;")
						.append("authorised;")
						.append("smartphone;")
						.append("fulfillDelivery;")
						.append("bankName;")
						.append("account_number;")
						.append("branch_code;")
						.append("training;")
						.append("isConvicted;")
						.append("haveCase;")
						.append("haveTreatment;")
						.toString();

				//create arrays for the data, and the fieldnames
				String[] valueArray;
				String[] fieldArray;
				String delimiter = ";";
				
				valueArray = regData.split(delimiter);
				fieldArray = myStringbuild.split(delimiter);
				
				for (int x = 0; x < valueArray.length; x++) {
					
						String fieldName = fieldArray[x];
						String myData = valueArray[x];
						myWait(1000);
						
						
						if(fieldName.equals("birthday")) {
							DP_DriverSignup.SelectBirthDate(driver, 3, "Date of Birth", myData, test);			
						}
						else if(fieldName.equals("training")){
							//DP_DriverSignup.SelectTrainingDate(driver, 13, "Date of Training Session", myData, test);			
						}
						else if(fieldName.equals("country")){
							//currently no need for this, as the country will default to Singapore			
						}				
						else if(fieldName.equals("gender")){
							DP_DriverSignup.clickRadioButtonGender(driver, myData, test);		
						}
						else if(fieldName.equals("race")){
							DP_DriverSignup.clickRadioButtonRace(driver, myData, test);	
						}
						else if(fieldName.equals("authorised")){
							DP_DriverSignup.clickRadioButtonCitizenship(driver, myData, test);	
						}
						else if(fieldName.equals("smartphone")){
							DP_DriverSignup.clickRadioButtonMobileType(driver, myData, test);	
						}
						else if(fieldName.equals("fulfillDelivery")){	
							DP_DriverSignup.selectFulfill(driver, myData, test);
						}
						else if(fieldName.equals("bankName")){
							DP_DriverSignup.selectBankName(driver, myData, test);
						}
						else if(fieldName.equals("isConvicted")){
							DP_DriverSignup.clickRadioButtonConvicted(driver, myData, test);
						}
						else if(fieldName.equals("haveCase")){
							DP_DriverSignup.clickRadioButtonCriminal(driver, myData, test);
						}
						else if(fieldName.equals("haveTreatment")){
							DP_DriverSignup.clickRadioButtonTreatment(driver, myData, test);
						}
						else {
							DP_DriverSignup.EnterText(driver, fieldName, myData, test);
						}			
				}
				
				//click "I declare"
				WebElement iDeclare = driver.findElement(By.xpath("//div[2]/div/div/fieldset/div[1]/div/label"));
				iDeclare.click();
				
				myWait(3000);
				//click "I Agree"
				//WebElement iAgree = driver.findElement(By.xpath("//div[6]/div[2]/div/div/fieldset/div[2]/div"));
				WebElement iAgree = driver.findElement(By.xpath("//div[@class='checkbox']//label//div"));
				//WebElement iAgree = driver.findElement(By.cssSelector(".form-group:nth-child(2) > .checkbox > label"));
				//WebElement iAgree = driver.findElement(By.name("agree_tnc"));
				//iAgree.click();
				
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", iAgree);
				
				myWait(2000);
				//The "Submit" will be enabled once i declare and I agree checkboxes were clicked
				WebElement iSubmit = driver.findElement(By.xpath("//div/form/div[7]/button"));
				if(iSubmit.isEnabled()) {
					iSubmit.click();

				}
				
				myWait(1000);
				

				
				//verify error msg is displayed
				String[] fieldError;
				String myFieldError = new StringBuilder()
						.append("Email;")
						.append("Password;")
						.append("Confirm Password;")
						.append("Phone;")
						.append("Postcode;")
						.append("Postcode;")
						.append("Emergency Contact Number;")
						.append("Last 4 characters of NRIC number;")
						.append("Bank Account Number;")
						.append("Branch Code;")
						.toString();
				
				fieldError = myFieldError.split(";");
				
				for (int x = 0; x < fieldError.length; x++) {			
					String fieldName = fieldError[x];	
					DP_DriverSignup.verifyErrorMessage(driver, fieldName, test);
					
				}
				
				driver.quit();	


			}
		
		}
		
	}

}

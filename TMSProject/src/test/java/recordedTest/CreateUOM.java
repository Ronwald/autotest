package recordedTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import utilities.MyUtilities;

public class CreateUOM extends MyUtilities {
	
	String driverLoc = "/usr/bin/chromedriver";
	
	
	@Test
	  public void test1CreateUOM() throws Exception {
		
		ExtentTest test = extent.createTest("RECORDED TESTCASE 1 : Test successful creation of UOM.");
		System.setProperty("webdriver.chrome.driver",driverLoc);
		WebDriver driver = new ChromeDriver(setupChromeOptions());
		
	    // ERROR: Caught exception [unknown command [#]]
	    driver.get("https://wms-stag.courex.com.sg/");
	    driver.findElement(By.name("username")).clear();
	    driver.findElement(By.name("username")).sendKeys("ronwald.sandoval@urbanfox.asia");
	    driver.findElement(By.name("password")).clear();
	    driver.findElement(By.name("password")).sendKeys("DpTestPassword1*");
	    driver.findElement(By.xpath("//button")).click();
	    driver.findElement(By.xpath("//table[@id='content']/tbody/tr/td[2]/form/table/tbody/tr[2]/td/span/a/span")).click();
	    driver.findElement(By.xpath("//input")).clear();
	    driver.findElement(By.xpath("//input")).sendKeys("Rxxxxxxxxxxxxxxl ( Courex DP - Account No: 00038677 )");
	    driver.findElement(By.id("ui-id-1")).click();
	    driver.findElement(By.xpath("//tr[3]/td")).click();
	    driver.findElement(By.id("submitlogin")).click();
	    driver.findElement(By.xpath("//li[2]/a/i")).click();
	    driver.findElement(By.linkText("Settings")).click();
	    switchTab(driver);
	   
	    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
	    driver.findElement(By.linkText("Units of Measurement Settings")).click();
	    
	    driver.findElement(By.linkText("Add Units of Measurement")).click();
	    driver.findElement(By.name("valueName")).click();
	    driver.findElement(By.name("valueName")).clear();
	    driver.findElement(By.name("valueName")).sendKeys("PIECES");
	    driver.findElement(By.name("addBtn")).click();
	    driver.findElement(By.linkText("Add Units of Measurement")).click();
	    driver.findElement(By.name("valueName")).click();
	    driver.findElement(By.name("valueName")).clear();
	    driver.findElement(By.name("valueName")).sendKeys("BAGS");
	    driver.findElement(By.name("addBtn")).click();
	    driver.findElement(By.linkText("Logout")).click();
	  }

}

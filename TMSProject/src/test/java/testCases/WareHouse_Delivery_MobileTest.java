package testCases;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pageObjects.MobileHome;
import utilities.MyUtilities;

public class WareHouse_Delivery_MobileTest extends MyUtilities {
	
	
	
	

	
	@Test(priority = 3)
	public void completeDelieveryML() throws Exception {
		
		test = extent.createTest("TESTCASE 1 : Test Unsuccessful logging into Mobile Delivery app");
		
		AppiumDriver<MobileElement> driver = MyUtilities.setupNativeMobile();

		myWait(3000);
		MobileHome.EnterText(driver, "false", username, test);
		MobileHome.EnterText(driver, "true", password, test);
		MobileHome.ClickImage(driver, "false", test);
		MobileHome.ClickButton(driver, "true", "SIGN IN", test);	
		test.log(Status.INFO, "User is successfully logged in");
		myWait(10000);	
		MobileHome.ClickTextViewViaIndex(driver, "TextView", 0, "Burger", test);
		MobileHome.ClickTextViewViaText(driver, "TextView", "Dev", test);
		myWait(3000);
		MobileHome.ClickTextViewViaText(driver, "TextView", "STAG", test);
		myWait(5000);	
		//login again after selecting the Test or Stag environment
		MobileHome.EnterText(driver, "false", driverUsername, test);
		MobileHome.EnterText(driver, "true", driverPassword, test);
		MobileHome.ClickImage(driver, "false", test);
		MobileHome.ClickButton(driver, "true", "SIGN IN", test);
		myWait(5000);
		//click Home
		//MobileHome.ClickAndroidViewViaIndex(driver, "ViewGroup", 1, "Home Button", test);
		//MobileHome.ClickTextViewViaIndex(driver, "TextView", 0, "Burger", test);
		//MobileHome.ClickTextViewViaIndex(driver, "ViewGroup", 0, "Home Button", test);
		MobileHome.ClickTextViewViaBounds(driver, "[293,1696][356,1761]", "Delivery Button", test);
		myWait(8000);
		MobileHome.EnterText(driver, "false", MLNumber, test);
		MobileHome.ClickButton(driver, "true", "Update", test);
		myWait(3000);
		
		MobileHome.scrollDown(driver);
		//MobileHome.myScroll(driver);
		
		myWait(5000);
		
		MobileHome.ClickButtonByText(driver, "On the way to deliver", test);
		myWait(3000);

		MobileHome.ClickButtonByText(driver, "Delivered", test);
		myWait(3000);
		MobileHome.EnterText(driver, "false", deliveryRecipient, test);
		
		myWait(5000);
		
		MobileHome.ClickButtonByText(driver, "SAVE", test);
		
		myWait(4000);
		
		//driver.quit();
		
	}

	

}
